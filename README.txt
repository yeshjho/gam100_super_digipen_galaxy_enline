ENlINE
(lower l, upper I)
- The game you enclose shapes with lines to conquer them and try to get the highest score among players.

Made by Super Digipen Galaxy
	Team members:
	- Joonho Hwang	(joonho.hwang)
	- Duhwan Kim	(duhwan.kim)
	- Sunghwan Cho	(sunghwan.cho)

Course: GAM100F19KR
Instructor: David Ly

All content © 2019 DigiPen (USA) Corporation, all rights reserved.

[Build Instructions]
- Build in Release configuration, no other special step needed.

[How to Play]
- The player encloses a shape(colors the last line of the shape) conquers the shape.
- The sum of the shapes' areas of each players are their scores.
- You get one extra turn if you conquered any shape on the last turn.
- When all cells are conquered, the player whose score is the highest wins.

[Game Controls]
- Click on the line you want to color.

[Credits]
- None.

[For Consideration]
- You can customize the functionalities of the buttons (onHovered/onClicked)
- [Insert random generation here]

[External Sources]
- [Insert the list of external code used in random generation here]
