/*
	CreditUI.h

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "UI.h"

namespace Enline
{
	class CreditUI : public UI
	{
	public:
		CreditUI(const std::map<UIButton*, ButtonGeometry>& buttons = {});

		virtual void draw() const override;
	};
}
