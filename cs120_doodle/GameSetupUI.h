/*
	GameSetupUI.h

	GAM100, Fall 2019

	JoonHo Hwang retouched the color part
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "UI.h"

namespace Enline
{
	class GameSetupUI : public UI
	{
	public:
		GameSetupUI(const std::map<UIButton*, ButtonGeometry>& buttons = {});

		virtual void draw() const override;


	public:
		unsigned char playerCount = 0;
	};
}

