/*
	PausedUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "PausedUI.h"
#include "global.h"

namespace Enline
{
	PausedUI::PausedUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{
		UIButton* resumeButton = new UIButton(150, 300, 100, 50, { LIGHT_SKY, "Resume" });
		resumeButton->allocateOnHovered([resumeButton]() { resumeButton->setDisplayAttributes({ HEAVY_SKY, " Resume" }); game.resumeTimer(); });
		resumeButton->allocateOnClicked([resumeButton]() { game.setCurrentState(EGameState::INGAME); });

		UIButton* toMainMenuButton = new UIButton(250, 300, 200, 50, { LIGHT_SKY, "To MainMenu" });
		toMainMenuButton->allocateOnHovered([toMainMenuButton]() { toMainMenuButton->setDisplayAttributes({ HEAVY_SKY, " To MainMenu" }); });
		toMainMenuButton->allocateOnClicked([toMainMenuButton]() { game.setCurrentState(EGameState::MAIN_MENU);  game.reset(); });

		UIButton* toGameSettingButton = new UIButton(450, 300, 200, 50, { LIGHT_SKY, "To Game Setup" });
		toGameSettingButton->allocateOnHovered([toGameSettingButton]() { toGameSettingButton->setDisplayAttributes({ HEAVY_SKY, " To Game Setup" }); });
		toGameSettingButton->allocateOnClicked([toGameSettingButton]() { game.setCurrentState(EGameState::GAME_SETUP); game.reset(); });

		registerButton(resumeButton);
		registerButton(toMainMenuButton);
		registerButton(toGameSettingButton);
	}

	void PausedUI::draw() const
	{
		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}