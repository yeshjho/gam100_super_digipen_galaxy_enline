﻿/*
	Game.h

	GAM100, Fall 2019

	JoonHo Hwang desinged the whole structure
	Sunghwan Cho implemented the UI setup part / getHoveredLine() / createDefaultMap() / EGameState
	Duhwan Kim implemented the createMap()

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include <map>
#include <deque>
#include <ctime>
#include <doodle/color.hpp>
#include <doodle/input.hpp>
#include "Point.h"
#include "UI.h"
#include "Timer.h"


namespace Enline
{
	class Cell;
	class Line;
	class Player;

	using ObjectID = size_t;
	using TrianglePoints = std::deque<std::deque<Point>>;

	enum class EGameState
	{
		MAIN_MENU,
		INGAME,
		PAUSED,
		HOW_TO_PLAY, // draw with the texture.
		CREDIT, // write credit and put it up
		GAME_SETUP,
		GAME_OVER
	};

	class Game
	{
	public:
		Game();
		~Game();

		void registerPlayer(std::string name, doodle::Color4ub color);

		ObjectID registerLine(float x, float y, float angleInRadian, float length);
		ObjectID registerLine(Point position, float angleInRadian, float length);
		ObjectID registerLine(Point startPoint, Point endPoint);

		ObjectID registerCell(const std::deque<ObjectID>& consistingLineIDs, const TrianglePoints& consistingTriangles);

		void createDefaultMap();
		void createMap(unsigned int seed = static_cast<unsigned int>(time(0)));

		void nextTurn();

		void update();
		void draw() const;

		void reset();
		void resetMap();

		Line* getLineByID(ObjectID id);
		Cell* getCellByID(ObjectID id);
		std::deque<ObjectID> getCellIDsByLineID(ObjectID id);
		std::deque<Cell*> getCellsByLineID(ObjectID id);

		void onMousePressed(doodle::MouseButtons button);
		void onTimeout();

		void pauseTimer();
		void resumeTimer();
		float getLeftTime() const noexcept;

		void setCurrentState(EGameState gameState);
		EGameState getCurrentState() const noexcept;

		size_t getPlayerCount() const noexcept;
		std::deque<Player*> getPlayers() const noexcept;
		size_t getCurrentPlayerIndex() const noexcept;

	private:
		Line* getHoveredLine();

		// Are all cells colored?
		bool checkGameEnd();
		void onGameEnd();


	public:
		static constexpr unsigned int DEFAULT_GRID_X_OFFSET = 250;
		static constexpr unsigned int DEFAULT_GRID_Y_OFFSET = 150;
		static constexpr unsigned int DEFAULT_GRID_HORIZONTAL_CELL_COUNT = 5;
		static constexpr unsigned int DEFAULT_GRID_VERTICAL_CELL_COUNT = 5;
		static constexpr unsigned int DEFAULT_GRID_CELL_WIDTH = 60;
		static constexpr unsigned int DEFAULT_GRID_CELL_HEIGHT = 60;

		// For random map generation
		static constexpr unsigned int MIN_POINT_COUNT = 20;
		static constexpr unsigned int MAX_POINT_COUNT = 30;
		static constexpr unsigned int MAX_SPREAD_MAGNITUDE = 10;
		static constexpr unsigned int TRIANGLE_ALGORITHM_COUNT = 4;
		static constexpr unsigned int MAX_TRIANGLE_COMBINE_POWER = 10;
		static constexpr unsigned int MAX_PRECOLORED_LINE_PORTION = 50;

		static constexpr float LINE_SELECTION_TOLERENCE = 10.f;
		
		static constexpr unsigned char MAX_PLAYER_COUNT = 8;

		static constexpr float TURN_TIME_LIMIT = 18.5f;

	private:
		EGameState currentState = EGameState::MAIN_MENU;

		std::map<ObjectID, Line*> linesByID;
		std::map<ObjectID, Cell*> cellsByID;
		std::map<ObjectID, std::deque<ObjectID>> cellsByLineID;

		std::deque<Player*> players;
		size_t currentPlayerIndex = 0;

		std::map<EGameState, UI*> uiByGameState;
		UI* currentUI = nullptr;

		// For pre-colored lines
		Player* fakePlayer;

		Timer timer{ TURN_TIME_LIMIT, [this]() { this->onTimeout(); }, false };
	};
}
