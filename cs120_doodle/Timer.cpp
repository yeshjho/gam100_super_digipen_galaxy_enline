/*
	Timer.cpp

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "Timer.h"
#include <doodle/doodle.hpp>


Timer::Timer(float amount, std::function<void(void)> onTimeout, bool doTick)
	: amount(amount), originalAmount(amount), onTimeout(onTimeout), doTick(doTick)
{}

void Timer::update()
{
	if (!doTick)
	{
		return;
	}

	amount -= doodle::DeltaTime;
	if (amount <= 0)
	{
		doTick = false;
		if (onTimeout)
		{
			onTimeout();
		}
	}
}

void Timer::pause()
{
	doTick = false;
}

void Timer::resume()
{
	doTick = true;
}

void Timer::reset()
{
	doTick = true;
	amount = originalAmount;
}

void Timer::reset(float newAmount)
{
	doTick = true;
	amount = newAmount;
}

float Timer::getLeftAmount() const noexcept
{
	return amount;
}
