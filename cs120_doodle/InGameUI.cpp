/*
	InGameUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <iostream>
#include <deque>
#include "doodle/drawing.hpp"
#include "doodle/color.hpp"
#include "Game.h"
#include "InGameUI.h"
#include "global.h"
#include "Player.h"


namespace Enline
{
	InGameUI::InGameUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{
		UIButton* pauseButton = new UIButton(350, 500, 100, 50, { LIGHT_SKY, "Pause" });
		pauseButton->allocateOnHovered([pauseButton]() { pauseButton->setDisplayAttributes({ HEAVY_SKY, " Pause" }); });
		pauseButton->allocateOnClicked([]() { game.setCurrentState(EGameState::PAUSED); game.pauseTimer(); });

		registerButton(pauseButton);
	}

	void InGameUI::draw() const
	{
		using namespace doodle;
		std::deque<Player*> listOfPlayers = game.getPlayers();

		for (int i = 0; i < game.getPlayerCount(); ++i)
		{
			push_settings();
			set_font_size(22);
			set_fill_color(listOfPlayers[i]->getColor());
			set_outline_color(0);
			draw_text(listOfPlayers[i]->getName() + "\n" + std::to_string(static_cast<int>(std::round(listOfPlayers[i]->getConqueredArea()))),
					i % 2 == 0 ? 20.f : 660.f, 100 * static_cast<float>((i / 2) + 1));
			pop_settings();
		}

		push_settings();
		set_outline_color(BLUE);
		no_fill();
		set_outline_width(3);
		int index = static_cast<int>(game.getCurrentPlayerIndex());
		draw_rectangle(index % 2 == 0 ? 0.f : 640.f, 100 * static_cast<float>((index / 2) + 1) - 30, 160, 50);
		pop_settings();

		draw_text(std::to_string(static_cast<int>(game.getLeftTime())), 380, 100);

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}
