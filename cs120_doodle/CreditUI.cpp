/*
	CreditUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "CreditUI.h"
#include "doodle/texture.hpp"
#include "doodle/drawing.hpp"
#include <iostream>
#include "game.h"
#include "global.h"

namespace Enline
{
	CreditUI::CreditUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{}

	void CreditUI::draw() const
	{
		constexpr float credit_Width = 800;
		constexpr float credit_Height = 500;

		doodle::Texture creditPNG;
		constexpr auto credit_texture_path = "PNGFiles/Credit.png";
		if (const bool loaded = creditPNG.LoadFromPNG(credit_texture_path); !loaded)
		{
			std::cerr << "Failed to load " << credit_texture_path << "\n";
		}
		doodle::draw_texture(creditPNG, 400, 300, credit_Width, credit_Height);

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}