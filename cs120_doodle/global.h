/*
	global.h

	GAM100, Fall 2019

	JoonHo Hwang wrote the rest
	Sunghwan Cho wrote the colors
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Game.h"
#include "doodle/color.hpp"


inline Enline::Game game{};

inline unsigned short LOGO_DISPLAY_FRAME = 144 * 3;

inline constexpr doodle::Color4ub LIGHT_SKY{ 106, 242, 218 };
inline constexpr doodle::Color4ub HEAVY_SKY{ 107, 124, 229 };
inline constexpr doodle::Color4ub LIGHT_GREEN{ 179, 251, 51 };
inline constexpr doodle::Color4ub BLUE{ 0, 0, 255 };
inline constexpr doodle::Color4ub GREY{ 139, 132, 132 };
inline constexpr doodle::Color4ub LIGHT_APRICOT{ 253, 194, 245 };