/*
	GameSetupUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang retouched the color part
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include <doodle/color.hpp>
#include <doodle/drawing.hpp>
#include "Game.h"
#include "GameSetupUI.h"
#include "global.h"
#include "helper_functions.h"


namespace Enline
{
	GameSetupUI::GameSetupUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{
		srand(static_cast<unsigned int>(time(0)));

		UIButton* playerSelctionButton = new UIButton(175, 150, 450, 50, { LIGHT_SKY, " Click to change amount of players" });
		playerSelctionButton->allocateOnHovered([playerSelctionButton]() { playerSelctionButton->setDisplayAttributes({ HEAVY_SKY, " Click to change amount of players" }); });
		playerSelctionButton->allocateOnClicked([this]() { ++(this->playerCount) %= (Game::MAX_PLAYER_COUNT + 1); });

		UIButton* normalGamePlayButton = new UIButton(300, 400, 100, 50, { LIGHT_SKY, "Normal" });
		normalGamePlayButton->allocateOnHovered([normalGamePlayButton]() { normalGamePlayButton->setDisplayAttributes({ HEAVY_SKY, "Normal" }); });
		normalGamePlayButton->allocateOnClicked([this]() { 
			if (this->playerCount > 1)
			{
				//To make sure all players' colors are different to each other
				unsigned char firstPlayerHue;
				const char HUE_OFFSET = 255 / this->playerCount;

				srand(static_cast<unsigned int>(time(0)));

				HSV hsv;
				hsv.Hue = rand() % 256;
				hsv.Saturation = (static_cast<double>(rand() % 71) + 30) / 100;
				hsv.Value = (static_cast<double>(rand() % 71) + 30) / 100;

				firstPlayerHue = hsv.Hue;
				game.registerPlayer("Player " + std::to_string(game.getPlayerCount() + 1), HSVToRGB(hsv));

				for (int i = 0; i < this->playerCount - 1; ++i)
				{
					hsv.Hue = (rand() % (firstPlayerHue + HUE_OFFSET * game.getPlayerCount() + 1) + firstPlayerHue + HUE_OFFSET * game.getPlayerCount() / 3) % 256;
					hsv.Saturation = static_cast<double>(static_cast<double>(rand() % 71) + 30) / 100;
					hsv.Value = static_cast<double>(static_cast<double>(rand() % 71) + 30) / 100;

					game.registerPlayer("Player " + std::to_string(game.getPlayerCount() + 1), HSVToRGB(hsv));
				}

				game.setCurrentState(EGameState::INGAME);
				game.createDefaultMap();

				this->playerCount = 0;
			}
			});

		UIButton* randomGamePlayButton = new UIButton(400, 400, 100, 50, { LIGHT_SKY, "Random" });
		randomGamePlayButton->allocateOnHovered([randomGamePlayButton]() { randomGamePlayButton->setDisplayAttributes({ HEAVY_SKY, "Random" }); });
		randomGamePlayButton->allocateOnClicked([this]() {
			if (this->playerCount > 1)
			{
				unsigned char firstPlayerHue;
				const char HUE_OFFSET = 255 / this->playerCount;

				srand(static_cast<unsigned int>(time(0)));

				HSV hsv;
				hsv.Hue = rand() % 256;
				hsv.Saturation = (static_cast<double>(rand() % 71) + 30) / 100;
				hsv.Value = (static_cast<double>(rand() % 71) + 30) / 100;

				firstPlayerHue = hsv.Hue;
				game.registerPlayer("Player " + std::to_string(game.getPlayerCount() + 1), HSVToRGB(hsv));

				for (int i = 0; i < this->playerCount - 1; ++i)
				{
					hsv.Hue = (rand() % (firstPlayerHue + HUE_OFFSET * game.getPlayerCount() + 1) + firstPlayerHue + HUE_OFFSET * game.getPlayerCount() / 3) % 256;
					hsv.Saturation = static_cast<double>(static_cast<double>(rand() % 71) + 30) / 100;
					hsv.Value = static_cast<double>(static_cast<double>(rand() % 71) + 30) / 100;

					game.registerPlayer("Player " + std::to_string(game.getPlayerCount() + 1), HSVToRGB(hsv));
				}

				game.setCurrentState(EGameState::INGAME);
				game.createMap();

				this->playerCount = 0;
			}
			});

		registerButton(playerSelctionButton);
		registerButton(normalGamePlayButton);
		registerButton(randomGamePlayButton);
	}

	void GameSetupUI::draw() const
	{
		doodle::draw_text(std::to_string(playerCount), 380, 350);

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}