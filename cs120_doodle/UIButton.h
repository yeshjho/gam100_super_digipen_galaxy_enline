/*
	UIButton.h

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include <string>
#include <functional>
#include "Point.h"
#include "doodle/color.hpp"


namespace Enline
{
	struct ButtonGeometry
	{
		Point position;
		float width, height;
	};

	struct ButtonDisplayAttributes
	{
		ButtonDisplayAttributes(doodle::Color4ub fillColor, std::string str)
			: fillColor(fillColor), str(str)
		{}

		bool doFill = true;
		doodle::Color4ub fillColor{ 255 };

		bool hasOutline = true;
		float outlineWidth = 1.f;
		doodle::Color4ub outlineColor{ 0 };

		std::string str;
		float strSize = 20.f;
		doodle::Color4ub strColor{ 0 };

		bool hasStrOutline = false;
		float strOutlineWidth = 1.f;
		doodle::Color4ub strOutlineColor{ 0 };
	};

	class UIButton
	{
	public:
		UIButton(Point position, float width, float height, const ButtonDisplayAttributes& displayAttributes);
		UIButton(float x, float y, float width, float height, const ButtonDisplayAttributes& displayAttributes);

		void draw() const;
		void update();

		void onHovered();
		void onClicked();

		void allocateOnHovered(std::function<void()> newFunction);
		void allocateOnClicked(std::function<void()> newFunction);

		void setDisplayAttributes(const ButtonDisplayAttributes& newAttributes);

		ButtonGeometry getGeometry() const noexcept;


	private:
		ButtonGeometry geometry;

		ButtonDisplayAttributes originalDisplayAttributes;
		ButtonDisplayAttributes displayAttributes;

		// Making other programmer able to customize what will be executed on hovered/clicked
		std::function<void()> toBeExecutedOnHovered = nullptr;
		std::function<void()> toBeExecutedOnClicked = nullptr;

		bool isHovered = false;
		bool wasHovered = false;
	};
}
