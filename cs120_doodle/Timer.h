/*
	Timer.h

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include <functional>


class Timer
{
public:
	Timer(float amount, std::function<void(void)> onTimeout, bool doTick = true);

	void update();

	void pause();
	void resume();

	void reset();
	void reset(float newAmount);

	float getLeftAmount() const noexcept;


private:
	bool doTick;
	float originalAmount;
	float amount;

	std::function<void(void)> onTimeout = nullptr;
};
