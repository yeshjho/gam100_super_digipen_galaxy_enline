/*
	Line.cpp

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <cmath>
#include <algorithm>
#include "doodle/doodle.hpp"
#include "Line.h"
#include "global.h"
#include "Cell.h"
#include "Player.h"


namespace Enline
{
	Line::Line(ObjectID id, Point startPoint, float angleInRadian, float length)
		: id(id), startPoint(startPoint), angleInRadian(angleInRadian), length(length)
	{
		endPoint = { startPoint.x + std::cos(angleInRadian) * length, startPoint.y + std::sin(angleInRadian) * length };
	}

	Line::Line(ObjectID id, Point startPoint, Point endPoint)
		: id(id), startPoint(startPoint), endPoint(endPoint)
	{
		const float xDifference = endPoint.x - startPoint.x;
		const float yDifference = endPoint.y - startPoint.y;
		angleInRadian = std::fmod(std::atan2f(yDifference, xDifference) + 2* doodle::PI,2 * doodle::PI);
		length = std::sqrtf(powf(xDifference, 2.f) + powf(yDifference, 2.f));
	}

	Line::Line(ObjectID id, float startX, float startY, float endX, float endY)
		: id(id), startPoint({ startX, startY }), endPoint({ endX, endY })
	{
		const float xDifference = endX - startX;
		const float yDifference = endY - startY;
		angleInRadian = std::atan2f(yDifference, xDifference);
		length = std::sqrtf(powf(xDifference, 2.f) + powf(yDifference, 2.f));
	}

	void Line::update()
	{
		wasHovered = isHovered;
		isHovered = false;
	}

	void Line::draw() const
	{
		using namespace doodle;
		push_settings();
		set_outline_color(wasHovered ? HOVERED_LINE_COLOR : ownedPlayer ? color : DEFAULT_LINE_COLOR);
		set_outline_width(wasHovered ? HOVERED_LINE_WIDTH : ownedPlayer ? COLORED_LINE_WIDTH : DEFAULT_LINE_WIDTH);
		draw_line(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
		pop_settings();
	}

	void Line::onHovered()
	{
		if (ownedPlayer)
		{
			return;
		}

		isHovered = true;
	}

	bool Line::onClicked(Player* clickedPlayer)
	{
		ownedPlayer = clickedPlayer;
		color = clickedPlayer->getColor();

		std::deque<bool> results;
		for (Cell* cell : game.getCellsByLineID(id))
		{
			results.push_back(cell->onLineClicked(clickedPlayer));
		}

		return std::any_of(results.begin(), results.end(), [](bool result) { return result; });
	}

	std::deque<Point> Line::getEndingPoints() const noexcept
	{
		return std::deque<Point>{ startPoint, endPoint };
	}

	float Line::getAngleInRadian() const noexcept
	{
		return std::fmodf(angleInRadian + 2 * doodle::PI ,2 * doodle::PI);
	}

	float Line::getLength() const noexcept
	{
		return length;
	}

	bool Line::isOwned() const noexcept
	{
		return ownedPlayer;
	}
}
