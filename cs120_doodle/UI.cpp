/*
	UI.cpp

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <utility>
#include "UI.h"


namespace Enline
{
	UI::UI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: buttons(buttons)
	{}

	UI::~UI()
	{
		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			delete button.first;
		}
	}

	void UI::update()
	{
		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->update();
		}
	}

	void UI::onHovered(float mouseX, float mouseY)
	{
		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			const ButtonGeometry geometry = button.second;

			if (geometry.position.x <= mouseX && mouseX <= geometry.position.x + geometry.width &&
				geometry.position.y <= mouseY && mouseY <= geometry.position.y + geometry.height)
			{
				button.first->onHovered();
			}
		}
	}

	void UI::onClicked(float mouseX, float mouseY)
	{
		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			const ButtonGeometry geometry = button.second;

			if (geometry.position.x <= mouseX && mouseX <= geometry.position.x + geometry.width &&
				geometry.position.y <= mouseY && mouseY <= geometry.position.y + geometry.height)
			{
				button.first->onClicked();
			}
		}
	}

	void UI::registerButton(UIButton* newButton)
	{
		buttons[newButton] = newButton->getGeometry();
	}
}
