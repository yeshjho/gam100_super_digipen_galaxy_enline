/*
	MainMenuUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "MainMenuUI.h"
#include "global.h"
#include "doodle/doodle.hpp"

namespace Enline
{
	MainMenuUI::MainMenuUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{
		UIButton* gameStartButton = new UIButton(325, 300, 150, 50, { LIGHT_SKY, "GameStart" });
		gameStartButton->allocateOnHovered([gameStartButton]() { gameStartButton->setDisplayAttributes({ HEAVY_SKY, " GameStart" }); });
		gameStartButton->allocateOnClicked([]() { game.setCurrentState(EGameState::GAME_SETUP); });

		UIButton* howToPlayButton = new UIButton(325, 350, 150, 50, { LIGHT_SKY, "HowToPlay" });
		howToPlayButton->allocateOnHovered([howToPlayButton]() { howToPlayButton->setDisplayAttributes({ HEAVY_SKY, " HowToPlay" }); });
		howToPlayButton->allocateOnClicked([]() { game.setCurrentState(EGameState::HOW_TO_PLAY); });

		UIButton* creditButton = new UIButton(325, 400, 150, 50, { LIGHT_SKY, "Credit" });
		creditButton->allocateOnHovered([creditButton]() { creditButton->setDisplayAttributes({ HEAVY_SKY, " Credit" }); });
		creditButton->allocateOnClicked([]() { game.setCurrentState(EGameState::CREDIT); });

		UIButton* exitButton = new UIButton(325, 450, 150, 50, { LIGHT_SKY, "Exit" });
		exitButton->allocateOnHovered([exitButton]() { exitButton->setDisplayAttributes({ LIGHT_APRICOT, " Exit" }); });
		exitButton->allocateOnClicked([]() { doodle::close_window(); });

		registerButton(gameStartButton);
		registerButton(creditButton);
		registerButton(howToPlayButton);
		registerButton(exitButton);
	}

	void MainMenuUI::draw() const
	{
		using namespace doodle;

		push_settings();
		set_outline_color(255);
		set_outline_width(1);
		set_font_size(100);
		set_fill_color({ 165, 237, 248 });
		draw_text("ENlINE", 200, 250);
		pop_settings();

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}