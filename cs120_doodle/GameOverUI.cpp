/*
	GameOverUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <iostream>
#include <algorithm>
#include "doodle/drawing.hpp"
#include "GameOverUI.h"
#include "Game.h"
#include "global.h"
#include "Player.h"

namespace Enline
{
	GameOverUI::GameOverUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{
		UIButton* toMainMenuButton = new UIButton(300, 500, 200, 50, { LIGHT_SKY, "To MainMenu" });
		toMainMenuButton->allocateOnHovered([toMainMenuButton]() { toMainMenuButton->setDisplayAttributes({ LIGHT_APRICOT, " To MainMenu" }); });
		toMainMenuButton->allocateOnClicked([]() { game.setCurrentState(EGameState::MAIN_MENU); game.reset(); });

		registerButton(toMainMenuButton);
	}

	void GameOverUI::draw() const
	{
		using namespace doodle;
		std::deque<Player*> listOfPlayers = game.getPlayers();
		std::sort(listOfPlayers.begin(), listOfPlayers.end(), [](Player* player1, Player* player2) { return player1->getConqueredArea() > player2->getConqueredArea(); });
		
		push_settings();
		set_font_size(30);
		draw_text("The Winner is:",330,100);
		draw_text(listOfPlayers[0]->getName() + " : " + std::to_string(static_cast<int>(std::round(listOfPlayers[0]->getConqueredArea()))), 100, 200);
		draw_text("\t\t\tCongratualations! \n\t\t Want to play again?",100,300);
		pop_settings();

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}