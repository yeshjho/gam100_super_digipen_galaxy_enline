/*
	Point.cpp

	GAM100, Fall 2019

	JoonHo Hwang helped organizing
	Sunghwan Cho
	Duhwan Kim wrote this all

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <cmath>
#include <algorithm>
#include "Vec2.h"


float Vec2::getAngle()
{
	return std::atan2(y, x);
}

void Vec2::setAngle(float angle)
{
	x = getLength() * std::cos(angle);
	y = getLength() * std::sin(angle);
}

float Vec2::getLength()
{
	return std::sqrtf(std::powf(x, 2) + std::powf(y, 2));
}

void Vec2::setLength(float length)
{
	x = length * std::cos(getAngle());
	y = length * std::sin(getAngle());
}

Vec2 Vec2::operator+(Vec2 v)
{
	return Vec2{ x + v.x, y + v.y };
}

Vec2 Vec2::operator-(Vec2 v)
{
	return Vec2{ x - v.x, y - v.y };
}

Vec2 Vec2::operator*(const float scalar)
{
	return Vec2{ x * scalar, y * scalar };
}

Vec2 Vec2::operator/(float scalar)
{
	return Vec2{ x / scalar, y / scalar };
}

Vec2& Vec2::operator+=(Vec2 v)
{
	x += v.x;
	y += v.y;
	return *this;
}

Vec2& Vec2::operator-=(Vec2 v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

Vec2& Vec2::operator*=(const float scalar)
{
	x *= scalar;
	y *= scalar;
	return *this;
}

Vec2& Vec2::operator/=(float scalar)
{
	x /= scalar;
	y /= scalar;
	return *this;
}

float Vec2::dot(Vec2 v)
{
	return x * v.x + y * v.y;
}

float getDistanceBetweenDots(Vec2 v1, Vec2 v2)
{
	const float x_diff = v1.x - v2.x;
	const float y_diff = v1.y - v2.y;
	return std::sqrtf(x_diff * x_diff + y_diff * y_diff);
}

float getDistanceBetweenDotsSquared(Vec2 v1, Vec2 v2)
{
	const float x_diff = v1.x - v2.x;
	const float y_diff = v1.y - v2.y;
	return x_diff * x_diff + y_diff * y_diff;
}

float getDistanceBetweenDotAndLineSegment(Vec2 p, Vec2 v, Vec2 w) {
	// Return minimum distance between line segment vw and point p
	const float l2 = getDistanceBetweenDotsSquared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
	if (l2 == 0.0) return getDistanceBetweenDots(p, v);   // v == w case
	// Consider the line extending the segment, parameterized as v + t (w - v).
	// We find projection of point p onto the line. 
	// It falls where t = [(p-v) . (w-v)] / |w-v|^2
	// We clamp t from [0,1] to handle points outside the segment vw.
	const float t = std::max(0.f, std::min(1.f, (p - v).dot(w - v) / l2));
	const Vec2 projection = v + (w - v) * t;  // Projection falls on the segment
	return getDistanceBetweenDots(p, projection);
}

float getDistanceBetweenDotAndLine(Vec2 p, Vec2 v, Vec2 w)
{
	float area = abs((v.x - p.x) * (w.y - p.y) - (v.y - p.y) * (w.x - p.x));
	float AB = sqrtf( powf(v.x - w.x, 2) + powf(v.y - w.y, 2));
	return (area / AB);
}
