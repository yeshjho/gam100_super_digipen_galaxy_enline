/*
	helper_functions.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho wrote this all
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <cmath>
#include <doodle/color.hpp>
#include "helper_functions.h"


doodle::Color4ub HSVToRGB(HSV inputHSV)
{
	doodle::Color4ub outputRGB;
	double C = inputHSV.Saturation * inputHSV.Value;
	double X = C * (1 - std::abs(std::fmod(inputHSV.Hue / 60.0, 2) - 1));
	double m = inputHSV.Value - C;
	double redScale, greenScale, blueScale;

	redScale = 120 <= inputHSV.Hue && inputHSV.Hue < 240 ? 0 : 60 <= inputHSV.Hue && inputHSV.Hue < 300 ? X : C;
	greenScale = 60 <= inputHSV.Hue && inputHSV.Hue < 180 ? C : 0 <= inputHSV.Hue && inputHSV.Hue < 240 ? X : 0;
	blueScale = 0 <= inputHSV.Hue && inputHSV.Hue < 120 ? 0 : 180 <= inputHSV.Hue && inputHSV.Hue < 300 ? C : X;

	outputRGB.red = static_cast<unsigned char>((redScale + m) * 255);
	outputRGB.green = static_cast<unsigned char>((greenScale + m) * 255);
	outputRGB.blue = static_cast<unsigned char>((blueScale + m) * 255);

	return outputRGB;
}
