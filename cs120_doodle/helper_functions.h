/*
	helper_functions.h

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho wrote this all
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once


namespace doodle
{
	class Color4ub;
}


typedef struct {
	unsigned char Hue;       // angle in degrees
	double Saturation;       // a fraction between 0 and 1
	double Value;       // a fraction between 0 and 1
} HSV;

doodle::Color4ub HSVToRGB(HSV inputHSV);
