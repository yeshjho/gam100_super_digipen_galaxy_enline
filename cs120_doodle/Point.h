/*
	Point.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once

struct Point
{
	float x;
	float y;

	bool operator==(Point p);
};