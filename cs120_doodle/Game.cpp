/*
	Game.cpp

	GAM100, Fall 2019

	JoonHo Hwang desinged the whole structure
	Sunghwan Cho implemented the UI setup part / getHoveredLine() / createDefaultMap() / EGameState
	Duhwan Kim implemented the createMap()

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <utility>
#include <random>
#include <algorithm>
#include <cmath>
#include <vector>
#include <doodle/doodle.hpp>
#include "Game.h"
#include "Line.h"
#include "Cell.h"
#include "Player.h"
#include "InGameUI.h"
#include "MainMenuUI.h"
#include "PausedUI.h"
#include "GameSetupUI.h"
#include "CreditUI.h"
#include "HowToPlayUI.h"
#include "global.h"
#include "GameOverUI.h"
#include "Vec2.h"


namespace Enline
{
	Game::Game() : fakePlayer(new Player("", Line::PRECOLORED_LINE_COLOR))
	{
		UIButton* goBackButton = new UIButton(350, 500, 100, 50, { LIGHT_GREEN, "Back" });
		goBackButton->allocateOnHovered([goBackButton]() { goBackButton->setDisplayAttributes({ LIGHT_APRICOT, " Back" }); });
		goBackButton->allocateOnClicked([]() { game.setCurrentState(EGameState::MAIN_MENU); });

		//MainMenu UI
		UI* mainMenuUI = new MainMenuUI;
		uiByGameState[EGameState::MAIN_MENU] = mainMenuUI;

		//HowToPlay UI
		UI* howToPlayUI = new HowToPlayUI;
		UIButton* goBackButton2 = new UIButton(*goBackButton);
		goBackButton2->allocateOnHovered([goBackButton2]() { goBackButton2->setDisplayAttributes({ LIGHT_APRICOT, " Back" }); });
		howToPlayUI->registerButton(goBackButton2);
		uiByGameState[EGameState::HOW_TO_PLAY] = howToPlayUI;

		//Credit UI
		UI* creditUI = new CreditUI;
		UIButton* goBackButton3 = new UIButton(*goBackButton);
		goBackButton3->allocateOnHovered([goBackButton3]() { goBackButton3->setDisplayAttributes({ LIGHT_APRICOT, " Back" }); });
		creditUI->registerButton(goBackButton3);
		uiByGameState[EGameState::CREDIT] = creditUI;

		//GameSetUp UI
		UI* gameSetupUI = new GameSetupUI;
		UIButton* goBackButton4 = new UIButton(*goBackButton);
		goBackButton4->allocateOnHovered([goBackButton4]() { goBackButton4->setDisplayAttributes({ LIGHT_APRICOT, " Back" }); });
		goBackButton4->allocateOnClicked([]() {game.reset(); game.setCurrentState(EGameState::MAIN_MENU); });
		gameSetupUI->registerButton(goBackButton4);
		uiByGameState[EGameState::GAME_SETUP] = gameSetupUI;

		//InGame UI
		UI* inGameUI = new InGameUI;
		uiByGameState[EGameState::INGAME] = inGameUI;

		//PausedUI
		UI* pausedUI = new PausedUI;
		uiByGameState[EGameState::PAUSED] = pausedUI;

		//GameOverUI
		UI* gameOverUI = new GameOverUI;
		uiByGameState[EGameState::GAME_OVER] = gameOverUI;

		setCurrentState(EGameState::MAIN_MENU);
	}

	Game::~Game()
	{
		for (std::pair<ObjectID, Line*> line : linesByID)
		{
			if (line.second)
			{
				delete line.second;
			}
		}

		for (std::pair<ObjectID, Cell*> cell : cellsByID)
		{
			if (cell.second)
			{
				delete cell.second;
			}
		}

		for (Player* player : players)
		{
			if (player)
			{
				delete player;
			}
		}

		delete fakePlayer;

		for (std::pair<EGameState, UI*> ui : uiByGameState)
		{
			delete ui.second;
		}
	}

	ObjectID Game::registerLine(float x, float y, float angleInRadian, float length)
	{
		const ObjectID id = linesByID.size();
		linesByID[id] = new Line(id, x, y, angleInRadian, length);

		return id;
	}

	ObjectID Game::registerLine(Point position, float angleInRadian, float length)
	{
		const ObjectID id = linesByID.size();
		linesByID[id] = new Line(id, position, angleInRadian, length);

		return id;
	}

	ObjectID Game::registerLine(Point startPoint, Point endPoint)
	{
		const ObjectID id = linesByID.size();
		linesByID[id] = new Line(id, startPoint, endPoint);

		return id;
	}

	ObjectID Game::registerCell(const std::deque<ObjectID>& consistingLineIDs, const TrianglePoints& consistingTriangles)
	{
		const ObjectID id = cellsByID.size();
		cellsByID[id] = new Cell(id, consistingLineIDs, consistingTriangles);

		for (ObjectID lineID : consistingLineIDs)
		{
			if (cellsByLineID.count(lineID))
			{
				cellsByLineID[lineID].push_back(id);
			}
			else
			{
				cellsByLineID[lineID] = std::deque<ObjectID>{ id };
			}
		}

		return id;
	}

	Line* Game::getHoveredLine()
	{
		using namespace std;

		const float mouseX = static_cast<float>(doodle::get_mouse_x());
		const float mouseY = static_cast<float>(doodle::get_mouse_y());
		float pointToMouseAngle = 0;
		float pointToMouseLength = 0;

		for (pair<ObjectID, Line*> existingLine : linesByID)
		{
			Line* line = existingLine.second;

			deque<Point> endingPoints = line->getEndingPoints();
			Point startingPoint = endingPoints[0];
			Point endingPoint = endingPoints[1];
			float lineAngle = line->getAngleInRadian();
			float lineLength = line->getLength();
			float xDifference = mouseX - startingPoint.x;
			float yDifference = mouseY - startingPoint.y;
			pointToMouseAngle = atan2f(yDifference, xDifference);
			pointToMouseLength = sqrtf((xDifference * xDifference) + (yDifference * yDifference));
			float slope = tanf(lineAngle);
			if (lineAngle > doodle::HALF_PI && lineAngle < doodle::PI)
			{
				if ((abs(slope * xDifference - yDifference) / sqrtf(slope * slope + 1)) <= LINE_SELECTION_TOLERENCE &&
					pointToMouseLength <= lineLength &&
					xDifference < 0 && 0 < yDifference
					)
				{
					return line;
				}
			}
			else if (lineAngle > doodle::PI && lineAngle < doodle::HALF_PI + doodle::PI)
			{
				if ((abs(slope * xDifference - yDifference) / sqrtf(slope * slope + 1)) <= LINE_SELECTION_TOLERENCE &&
					pointToMouseLength <= lineLength &&
					xDifference < 0 && yDifference < 0
					)
				{
					return line;
				}
			}
			else if(lineAngle > doodle::HALF_PI + doodle::PI && lineAngle < 2 * doodle::PI)
			{
				if ((abs(slope * xDifference - yDifference) / sqrtf(slope * slope + 1)) <= LINE_SELECTION_TOLERENCE &&
					pointToMouseLength <= lineLength &&
					0 < xDifference && yDifference < 0
					)
				{
					return line;
				}
			}
			else
			{
				if (pointToMouseLength <= lineLength && 
					(abs(slope * xDifference - yDifference) / sqrtf(slope * slope + 1)) <= LINE_SELECTION_TOLERENCE && 
					-LINE_SELECTION_TOLERENCE < xDifference && -LINE_SELECTION_TOLERENCE < yDifference
					)
				{
					return line;
				}

			}
		}

		return nullptr;
	}

	bool Game::checkGameEnd()
	{
		for (std::pair<ObjectID, Cell*> cell : cellsByID)
		{
			if (!cell.second->isOwned())
			{
				return false;
			}
		}

		return true;
	}

	void Game::onGameEnd()
	{
		setCurrentState(EGameState::GAME_OVER);

		timer.reset();
		timer.pause();
	}

	void Game::registerPlayer(std::string name, doodle::Color4ub color)
	{
		players.push_back(new Player(name, color));
	}

	void Game::createDefaultMap()
	{
#pragma warning(push)
		// arithmetic overflow won't happen here, disabling
#pragma warning(disable: 26451)
		using namespace std;
		vector<Point> points = {}; 
		vector<ObjectID> horizontalLineIDs = {}; 
		vector<ObjectID> verticalLineIDs = {}; 
		vector<deque<ObjectID>> lineIDs = {};
		vector<TrianglePoints> trianglepoints = {};


		//Generate points
		for (float y = 0.f; y < DEFAULT_GRID_VERTICAL_CELL_COUNT + 1; ++y)
		{
			for (float x = 0.f; x < DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1; ++x)
			{
				points.push_back(Point{DEFAULT_GRID_X_OFFSET + x * DEFAULT_GRID_CELL_WIDTH, DEFAULT_GRID_Y_OFFSET + y * DEFAULT_GRID_CELL_HEIGHT });
			}
		}

		//Generate&Register horizontal lines
		for (int x = 0; x < DEFAULT_GRID_VERTICAL_CELL_COUNT; ++x)
		{
			for (int y = 0; y < DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1; ++y)
			{
				horizontalLineIDs.push_back(registerLine(points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)], points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x + 1)]));
			}
		}

		//Generate&Register vertical lines
		for (int y = 0; y < DEFAULT_GRID_VERTICAL_CELL_COUNT; ++y)
		{
			for (int x = 0; x < DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1; ++x)
			{
				verticalLineIDs.push_back(registerLine(points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)], points[((y + 1) * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)]));
			}
		}

		//Register triangle points
		for (int y = 0; y < DEFAULT_GRID_VERTICAL_CELL_COUNT; ++y)
		{
			for (int x = 0; x < DEFAULT_GRID_HORIZONTAL_CELL_COUNT; ++x)
			{
				deque<Point> upperTrianglePoint; // one set of upper triangle point
				deque<Point> lowerTrianglePoint; // one set of lower triangle point
				TrianglePoints setOfTrianglePoints; // one set of trianlge points.

				upperTrianglePoint.push_back(points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)]);
				upperTrianglePoint.push_back(points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + (x + 1))]);
				upperTrianglePoint.push_back(points[((y + 1) * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)]);

				lowerTrianglePoint.push_back(points[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + (x + 1))]);
				lowerTrianglePoint.push_back(points[((y + 1) * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)]);
				lowerTrianglePoint.push_back(points[((y + 1) * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + (x + 1))]);

				setOfTrianglePoints.push_back(upperTrianglePoint);
				setOfTrianglePoints.push_back(lowerTrianglePoint);

				trianglepoints.push_back(setOfTrianglePoints);
			}
		}

		//Gathering&Organizing the lines in one container.
		for (int y = 0; y < DEFAULT_GRID_VERTICAL_CELL_COUNT; ++y)
		{
			for (int x = 0; x < DEFAULT_GRID_HORIZONTAL_CELL_COUNT; ++x)
			{
				if (!((y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x) % (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) == DEFAULT_GRID_HORIZONTAL_CELL_COUNT))
				{
					deque<ObjectID> consistingLineIDs = {}; // register cells
					consistingLineIDs.push_back(verticalLineIDs[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x)]);
					consistingLineIDs.push_back(verticalLineIDs[(y * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + x + 1)]);
					consistingLineIDs.push_back(horizontalLineIDs[(x * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + y)]);
					consistingLineIDs.push_back(horizontalLineIDs[(x * (DEFAULT_GRID_HORIZONTAL_CELL_COUNT + 1) + y + 1)]);
					lineIDs.push_back(consistingLineIDs);
				}
			}
		}

		for (int y = 0; y < DEFAULT_GRID_VERTICAL_CELL_COUNT; ++y)
		{
			for (int x = 0; x < DEFAULT_GRID_HORIZONTAL_CELL_COUNT; ++x)
			{
				registerCell(lineIDs[(y * DEFAULT_GRID_HORIZONTAL_CELL_COUNT + x)], trianglepoints[(y * DEFAULT_GRID_HORIZONTAL_CELL_COUNT + x)]);
			}
		}

	#pragma warning(pop)

		timer.resume();
	}

	void Game::createMap(unsigned int seed)
	{
		srand(seed);
		const unsigned int pointCount = rand() % (MAX_POINT_COUNT - MIN_POINT_COUNT + 1) + MIN_POINT_COUNT;

		const unsigned int SCREEN_SCALE = doodle::Width * 3 / 5;
		const unsigned int SCREEN_START_POSX = SCREEN_SCALE / 2;
		const unsigned int SCREEN_START_POSY = SCREEN_SCALE / 2;

		float root_pointCount = sqrtf(static_cast<float>(pointCount));
		if (root_pointCount != int(root_pointCount)) { ++root_pointCount; }

		int minInterval = 30;
		int maxInterval = int(SCREEN_SCALE / root_pointCount) - minInterval;

		Point tempPoint;
		float &tempPointX = tempPoint.x;
		float &tempPointY = tempPoint.y;

		std::deque<Point> single_triangle;
		std::deque<std::deque<Point>> triangles;

		// Mother triangle
		float single_rotate_angle = (rand() % 31415) * 0.0001f;
		for (int i = 0; i < 3; ++i)
		{
			float single_interval = rand() % (maxInterval - minInterval + 1) + static_cast<float>(minInterval); // Div Zero error check
			single_triangle.push_back( Point{ SCREEN_START_POSX + single_interval * cos(single_rotate_angle),
											  SCREEN_START_POSY + single_interval * sin(single_rotate_angle)}
									  );
			single_rotate_angle += (doodle::PI / 2);

		}
		triangles.push_back(single_triangle);
		single_triangle.clear();

		// Point Generating loop starts
		for (unsigned int i = 0; i < pointCount; ++i)
		{
			while (true)
			{
				// Set new point's location randomly
				tempPointX = static_cast<float>(rand() % SCREEN_SCALE) + doodle::Width / 5;
				tempPointY = static_cast<float>(rand() % SCREEN_SCALE) + 10;

				float alpha;
				float beta;
				float gamma;
				int correctCaseCount = 0;

				// Check temp point is inside of other triangles
				for (std::deque<Point> triangle : triangles)
				{
					alpha = ((triangle[1].y - triangle[2].y) * (tempPointX - triangle[2].x) + (triangle[2].x - triangle[1].x) * (tempPointY - triangle[2].y)) /
							((triangle[1].y - triangle[2].y) * (triangle[0].x - triangle[2].x) + (triangle[2].x - triangle[1].x) * (triangle[0].y - triangle[2].y));
					
					beta = ((triangle[2].y - triangle[0].y) * (tempPointX - triangle[2].x) + (triangle[0].x - triangle[2].x) * (tempPointY - triangle[2].y)) /
						   ((triangle[1].y - triangle[2].y) * (triangle[0].x - triangle[2].x) + (triangle[2].x - triangle[1].x) * (triangle[0].y - triangle[2].y));
					
					gamma = 1.0f - alpha - beta;

					// If the point is inside of any triangle
					if ((alpha > 0 && beta > 0 && gamma > 0)) { break; }
					++correctCaseCount;
				}

				// Confirm temp point when it is out of all triangles
				if (correctCaseCount == triangles.size()) { break; }
			}

			struct closestPointInfo
			{
				int triangle_index;
				int point_index;
				float distance;

				void sync_closestPoint(int triangle, int point, float dis)
				{
					triangle_index = triangle;
					point_index = point;
					distance = dis;
				}
			};

			closestPointInfo closestPoint{ 0, 0, static_cast<float>(SCREEN_SCALE) };
			int triangleIndex = 0;

			// Check all triangles and find the closest line segment from the temp point
			for (std::deque<Point> triangle : triangles)
			{
				for (int point_index = 0; point_index < 3; ++point_index)
				{
					int point_index2 = (point_index + 1) % 3;
					float cur_distance = getDistanceBetweenDotAndLineSegment(
						Vec2{ tempPoint.x, tempPoint.y }, 
						Vec2{ triangle[point_index].x, triangle[point_index].y },
						Vec2{ triangle[point_index2].x, triangle[point_index2].y});

					if (cur_distance < closestPoint.distance)
					{
						closestPoint.sync_closestPoint(triangleIndex, point_index, cur_distance);
					}

					else if (cur_distance == closestPoint.distance)
					{
						float distanceBetweenLine_origin;
						float distanceBetweenLine_new;

						distanceBetweenLine_origin = getDistanceBetweenDotAndLine(
							Vec2{ tempPoint.x, tempPoint.y },
							Vec2{ triangles[closestPoint.triangle_index][closestPoint.point_index].x,			triangles[closestPoint.triangle_index][closestPoint.point_index].y},
							Vec2{ triangles[closestPoint.triangle_index][(closestPoint.point_index + 1) % 3].x, triangles[closestPoint.triangle_index][(closestPoint.point_index + 1) % 3].y});

						distanceBetweenLine_new = getDistanceBetweenDotAndLine(
							Vec2{ tempPoint.x, tempPoint.y },
							Vec2{ triangle[point_index].x, triangle[point_index].y },
							Vec2{ triangle[point_index2].x, triangle[point_index2].y });

						if (distanceBetweenLine_new > distanceBetweenLine_origin) 
						{
							closestPoint.sync_closestPoint(triangleIndex, point_index, cur_distance);
						}
					}
				}
				triangleIndex++;
			}

			single_triangle.push_back(triangles[closestPoint.triangle_index][closestPoint.point_index]);
			single_triangle.push_back(triangles[closestPoint.triangle_index][(closestPoint.point_index+1) % 3]);
			single_triangle.push_back(tempPoint);
			
			// Avoid the case that point is too closest to line segment
			if (closestPoint.distance >= 30) 
			{
				int correctCaseCount = 0;
				float checkingPointPosX;
				float checkingPointPosY;
				
				// Before confirm the triangle, double check all other points are out of temp triangle
				for (std::deque<Point> triangle : triangles)
				{
					for (Point p : triangle)
					{
						checkingPointPosX = p.x;
						checkingPointPosY = p.y;
						
						float alpha;
						float beta;
						float gamma;
							alpha = ((single_triangle[1].y - single_triangle[2].y) * (checkingPointPosX - single_triangle[2].x) + (single_triangle[2].x - single_triangle[1].x) * (checkingPointPosY - single_triangle[2].y)) /
								((single_triangle[1].y - single_triangle[2].y) * (single_triangle[0].x - single_triangle[2].x) + (single_triangle[2].x - single_triangle[1].x) * (single_triangle[0].y - single_triangle[2].y));

							beta = ((single_triangle[2].y - single_triangle[0].y) * (checkingPointPosX - single_triangle[2].x) + (single_triangle[0].x - single_triangle[2].x) * (checkingPointPosY - single_triangle[2].y)) /
								((single_triangle[1].y - single_triangle[2].y) * (single_triangle[0].x - single_triangle[2].x) + (single_triangle[2].x - single_triangle[1].x) * (single_triangle[0].y - single_triangle[2].y));

							gamma = 1.0f - alpha - beta;

							// If the point is inside of any triangle
							if ((alpha > 0 && beta > 0 && gamma > 0)) { break; }
							++correctCaseCount;
					}
				}
				if (correctCaseCount == triangles.size() * triangles[0].size()) { triangles.push_back(single_triangle); }
				else { --i; }
			}
			else
			{
				--i;
			}

			single_triangle.clear();
			closestPoint.distance = static_cast<float>(SCREEN_SCALE);
		}
		// Point generating loop ends

		std::map<std::string, ObjectID> registeredLine;

		std::deque<Point> checkingLine;
		std::string registeredLineID;
		std::deque<ObjectID> triangleMakingLineIDs;
		std::deque<std::deque<Point>> single_cell;

		// Register each lines and cells
		for (std::deque<Point> triangle : triangles)
		{
			for (int i = 0; i < 3; ++i)
			{
				checkingLine.push_back(triangle[i]);
				checkingLine.push_back(triangle[(i + 1) % 3]);
				registeredLineID = std::to_string(checkingLine[0].x) + std::to_string(checkingLine[0].y) +
								   std::to_string(checkingLine[1].x) + std::to_string(checkingLine[1].y);

				if (registeredLine.count(registeredLineID) > 0)
				{
					triangleMakingLineIDs.push_back(registeredLine[registeredLineID]);
				}
				else
				{
					ObjectID cur_lineID = registerLine(checkingLine[0], checkingLine[1]);
					registeredLine.insert(make_pair(registeredLineID, cur_lineID));

					registeredLineID = std::to_string(checkingLine[1].x) + std::to_string(checkingLine[1].y) +
									   std::to_string(checkingLine[0].x) + std::to_string(checkingLine[0].y);

					registeredLine.insert(make_pair(registeredLineID, cur_lineID));

					triangleMakingLineIDs.push_back(cur_lineID);
				}
				single_triangle.push_back(triangle[i]);
				checkingLine.clear();
			}
			single_cell.push_back(single_triangle);
			registerCell(triangleMakingLineIDs, single_cell);

			triangleMakingLineIDs.clear();
			single_triangle.clear();
			single_cell.clear();
			checkingLine.clear();
			registeredLineID = "";
		}

		// Randomly fill some lines.
		for (unsigned int i = 0; i < pointCount / 7; ++i)
		{
			Line* randomLine = nullptr;
			while (!randomLine || randomLine->isOwned())
			{
				randomLine = linesByID[std::rand() % linesByID.size()];
			}

			randomLine->onClicked(fakePlayer);
		}

		timer.resume();
	}

	void Game::nextTurn()
	{
		++currentPlayerIndex %= players.size();
	}

	void Game::update()
	{
		// NOTE: ORDER MATTERS!
		if (currentState == EGameState::INGAME)
		{
			Line* hoveredLine = getHoveredLine();
			if (hoveredLine)
			{
				getHoveredLine()->onHovered();
			}
		}
		if (currentUI)
		{
			currentUI->onHovered(static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()));
		}

		if (currentState == EGameState::INGAME)
		{
			for (std::pair<ObjectID, Line*> line : linesByID)
			{
				if (line.second)
				{
					line.second->update();
				}
			}
		}
		if (currentUI)
		{
			currentUI->update();
		}

		timer.update();
	}

	void Game::draw() const
	{
		if (currentState == EGameState::GAME_SETUP || currentState == EGameState::GAME_OVER)
		{
			doodle::clear_background(doodle::HexColor{ 0xBF8679FF });
		}

		if (currentState == EGameState::INGAME)
		{
			doodle::clear_background(doodle::HexColor{ 0xBF8679FF });
			for (std::pair<ObjectID, Cell*> cell : cellsByID)
			{
				if (cell.second)
				{
					cell.second->draw();
				}
			}

			for (std::pair<ObjectID, Line*> line : linesByID)
			{
				if (line.second)
				{
					line.second->draw();
				}
			}
		}

		if (currentUI)
		{
			currentUI->draw();
		}
	}

	void Game::reset()
	{
		linesByID.clear();
		cellsByID.clear();
		cellsByLineID.clear();

		players.clear();
		currentPlayerIndex = 0;

		timer.reset();
		timer.pause();
	}

	void Game::resetMap()
	{
		linesByID.clear();
		cellsByID.clear();
		cellsByLineID.clear();

		std::deque<Player*> newPlayers;
		for (Player* player : players)
		{
			newPlayers.push_back(new Player(*player));
		}
		players = newPlayers;
		currentPlayerIndex = 0;

		timer.reset();
		timer.pause();
	}

	Line* Game::getLineByID(ObjectID id)
	{
		return linesByID.at(id);
	}

	Cell* Game::getCellByID(ObjectID id)
	{
		return cellsByID.at(id);
	}

	std::deque<ObjectID> Game::getCellIDsByLineID(ObjectID id)
	{
		return cellsByLineID.at(id);
	}

	std::deque<Cell*> Game::getCellsByLineID(ObjectID id)
	{
		std::deque<Cell*> to_return;
		for (ObjectID cellID : getCellIDsByLineID(id))
		{
			to_return.push_back(getCellByID(cellID));
		}
		return to_return;
	}

	void Game::onMousePressed(doodle::MouseButtons)
	{
		if (currentState == EGameState::INGAME)
		{
			Line* hoveredLine = getHoveredLine();
			if (hoveredLine && !hoveredLine->isOwned())
			{
				if (!hoveredLine->onClicked(players[currentPlayerIndex]))
				{
					nextTurn();
				}
				timer.reset();
			}
			if (checkGameEnd())
			{
				onGameEnd();
			}
		}

		if (currentUI)
		{
			currentUI->onClicked(static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()));
		}
	}

	void Game::onTimeout()
	{
		Line* randomLine = nullptr;
		while (!randomLine || randomLine->isOwned())
		{
			randomLine = linesByID[std::rand() % linesByID.size()];
		}

		if (!randomLine->onClicked(players[currentPlayerIndex]))
		{
			nextTurn();
		}

		timer.reset();

		if (checkGameEnd())
		{
			onGameEnd();
		}
	}

	void Game::pauseTimer()
	{
		timer.pause();
	}

	void Game::resumeTimer()
	{
		timer.resume();
	}

	void Game::setCurrentState(EGameState newState)
	{
		currentState = newState;
		currentUI = uiByGameState[newState];
	}

	EGameState Game::getCurrentState() const noexcept
	{
		return currentState;
	}

	size_t Game::getPlayerCount() const noexcept
	{
		return players.size();
	}

	std::deque<Player*> Game::getPlayers() const noexcept
	{
		return players;
	}

	size_t Game::getCurrentPlayerIndex() const noexcept
	{
		return currentPlayerIndex;
	}

	float Game::getLeftTime() const noexcept
	{
		return timer.getLeftAmount();
	}
}
