/*
	Line.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Point.h"
#include "Game.h"


namespace Enline
{
	class Line
	{
	public:
		void update();
		void draw() const;

		void onHovered();
		// Returns whether this clicked resulted any cell to be colored or not.
		bool onClicked(Player* clickedPlayer);

		std::deque<Point> getEndingPoints() const noexcept;
		float getAngleInRadian() const noexcept;
		float getLength() const noexcept;
		bool isOwned() const noexcept;

	private:
		// Don't directly construct an instance of Line. Use Game's registerLine().
		friend ObjectID Game::registerLine(float x, float y, float angleInRadian, float length);
		friend ObjectID Game::registerLine(Point position, float angleInRadian, float length);
		friend ObjectID Game::registerLine(Point startPoint, Point endPoint);
		Line(ObjectID id, Point startPoint, float angleInRadian, float length);
		Line(ObjectID id, Point startPoint, Point endPoint);
		Line(ObjectID id, float startX, float startY, float endX, float endY);


	public:
		static constexpr float DEFAULT_LINE_WIDTH = 2;
		static constexpr float HOVERED_LINE_WIDTH = 4;
		static constexpr float COLORED_LINE_WIDTH = 5;
		static constexpr doodle::Color4ub DEFAULT_LINE_COLOR{ 80 };
		static constexpr doodle::Color4ub HOVERED_LINE_COLOR{ 255, 0, 0 };
		static constexpr doodle::Color4ub PRECOLORED_LINE_COLOR{ 0 };

	private:
		const ObjectID id;

		Point startPoint;
		Point endPoint;
		float angleInRadian;
		float length;

		Player* ownedPlayer = nullptr;
		doodle::Color4ub color = DEFAULT_LINE_COLOR;

		bool isHovered = false;
		bool wasHovered = false;
	};
}
