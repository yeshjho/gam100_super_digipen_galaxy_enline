/*
	Cell.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Point.h"
#include "Game.h"


namespace Enline
{
	class Cell
	{
	public:
		void draw() const;

		// Returns whether this click resulted this cell to be colored or not.
		bool onLineClicked(Player* clickedPlayer);
		bool isOwned() const noexcept;

		float getArea() const noexcept;

	private:
		// Don't directly construct an instance of Cell. Use Game's registerCell().
		friend ObjectID Game::registerCell(const std::deque<ObjectID>& consistingLineIDs, const TrianglePoints& consistingTriangles);
		Cell(ObjectID id, const std::deque<ObjectID>& consistingLineIDs, const TrianglePoints& consistingTriangles);

		float calculateArea() const;


	public:
		static constexpr doodle::Color4ub DEFAULT_CELL_COLOR{ 255 };

	private:
		const ObjectID id;

		std::deque<ObjectID> consistingLineIDs;
		TrianglePoints consistingTriangles;
		float area = 0;

		Player* ownedPlayer = nullptr;
		doodle::Color4ub color = DEFAULT_CELL_COLOR;
	};
}
