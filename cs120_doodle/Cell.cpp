/*
	Cell.cpp

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <cmath>
#include <algorithm>
#include <doodle/doodle.hpp>
#include "Cell.h"
#include "Line.h"
#include "Player.h"
#include "global.h"


namespace Enline
{
	Cell::Cell(ObjectID id, const std::deque<ObjectID>& consistingLineIDs, const TrianglePoints& consistingTriangles)
		: id(id), consistingLineIDs(consistingLineIDs), consistingTriangles(consistingTriangles), area(calculateArea())
	{}

	void Cell::draw() const
	{
		using namespace doodle;

		push_settings();
		no_outline();
		set_fill_color(color);
		for (std::deque<Point> points : consistingTriangles)
		{
			draw_triangle(points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y);
		}
		pop_settings();
	}

	bool Cell::onLineClicked(Player* clickedPlayer)
	{
		for (ObjectID lineId : consistingLineIDs)
		{
			if (!game.getLineByID(lineId)->isOwned())
			{
				return false;
			}
		}

		ownedPlayer = clickedPlayer;
		color = clickedPlayer->getColor();
		color.red = color.red - 20 < 0? 0 : color.red - 20;
		color.green = color.green - 20 < 0 ? 0 : color.green - 20;
		color.blue = color.blue - 20 < 0 ? 0 : color.blue - 20;
		clickedPlayer->onConqueredCell(area);

		return true;
	}

	bool Cell::isOwned() const noexcept
	{
		return ownedPlayer;
	}

	float Cell::getArea() const noexcept
	{
		return area;
	}

	float Cell::calculateArea() const
	{
		float sum = 0;
		for (std::deque<Point> points : consistingTriangles)
		{
			sum += std::abs(points[0].x * (points[1].y - points[2].y)
				+ points[1].x * (points[2].y - points[0].y)
				+ points[2].x * (points[0].y - points[1].y))
				/ 2.f;
		}

		return sum;
	}
}
