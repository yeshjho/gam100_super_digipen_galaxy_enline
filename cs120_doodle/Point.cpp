/*
	Point.cpp

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "Point.h"

bool Point::operator==(Point p)
{
	return x == p.x && y == p.y;
}
