/*
	Player.h

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include <string>
#include "Game.h"


namespace Enline
{
	class Player
	{
	public:
		Player(Player& player);

		void onConqueredCell(float area) noexcept;

		std::string getName() const noexcept;
		doodle::Color4ub getColor() const noexcept;
		float getConqueredArea() const noexcept;

	private:
		// Don't directly construct an instance of Player. Use Game's registerPlayer().
		friend void Game::registerPlayer(std::string name, doodle::Color4ub color);
		friend Game::Game();	// For fake player creation
		Player(std::string name, doodle::Color4ub color);


	private:
		std::string name;
		doodle::Color4ub color;

		float conqueredArea = 0;
	};
}
