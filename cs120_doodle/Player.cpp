/*
	Player.cpp

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "Player.h"


namespace Enline
{
	Player::Player(std::string name, doodle::Color4ub color) : name(name), color(color) {}

	Player::Player(Player& player)
	{
		name = player.name;
		color = player.color;
	}

	void Player::onConqueredCell(float area) noexcept
	{
		conqueredArea += area;
	}

	std::string Player::getName() const noexcept
	{
		return name;
	}

	doodle::Color4ub Player::getColor() const noexcept
	{
		return color;
	}

	float Player::getConqueredArea() const noexcept
	{
		return conqueredArea;
	}
}
