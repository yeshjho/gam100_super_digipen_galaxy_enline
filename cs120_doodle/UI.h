/*
	UI.h

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include <map>
#include "UIButton.h"


namespace Enline
{
	class UI
	{
	public:
		UI(const std::map<UIButton*, ButtonGeometry>& buttons = {});
		virtual ~UI();

		virtual void draw() const = 0;
		virtual void update();

		virtual void onHovered(float x, float y);
		virtual void onClicked(float x, float y);

		virtual void registerButton(UIButton* newButton);


	protected:
		std::map<UIButton*, ButtonGeometry> buttons;
	};
}
