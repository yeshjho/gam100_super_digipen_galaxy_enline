/*
	HowToPlayUI.cpp

	GAM100, Fall 2019

	JoonHo Hwang
	Sunghwan Cho implemented this based on Joonho Hwang's base class
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include "HowToPlayUI.h"
#include "doodle/texture.hpp"
#include "doodle/drawing.hpp"
#include <iostream>
#include "game.h"
#include "global.h"

namespace Enline
{
	HowToPlayUI::HowToPlayUI(const std::map<UIButton*, ButtonGeometry>& buttons)
		: UI(buttons)
	{}

	void HowToPlayUI::draw() const
	{
		constexpr float howToPlay_Width = 800;
		constexpr float howToPlay_Height = 500;

		doodle::Texture howToPlayPNG;
		constexpr auto howToPlay_texture_path = "PNGFiles/HowToPlay.png";
		if (const bool loaded = howToPlayPNG.LoadFromPNG(howToPlay_texture_path); !loaded)
		{
			std::cerr << "Failed to load " << howToPlay_texture_path << "\n";
		}
		doodle::draw_texture(howToPlayPNG, 400, 300, howToPlay_Width, howToPlay_Height);

		for (std::pair<UIButton*, ButtonGeometry> button : buttons)
		{
			button.first->draw();
		}
	}
}