/*
	UIButton.cpp

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <doodle/doodle.hpp>
#include "UIButton.h"


namespace Enline
{
	UIButton::UIButton(Point position, float width, float height, const ButtonDisplayAttributes& displayAttributes)
		: geometry({ position, width, height }), originalDisplayAttributes(displayAttributes), displayAttributes(displayAttributes)
	{}
	
	UIButton::UIButton(float x, float y, float width, float height, const ButtonDisplayAttributes& displayAttributes)
		: geometry({ { x, y }, width, height }), originalDisplayAttributes(displayAttributes), displayAttributes(displayAttributes)
	{}

	void UIButton::draw() const
	{
		using namespace doodle;
		push_settings();
		if (displayAttributes.doFill)
		{
			set_fill_color(displayAttributes.fillColor);
		}
		else
		{
			no_fill();
		}

		if (displayAttributes.hasOutline)
		{
			set_outline_width(displayAttributes.outlineWidth);
			set_outline_color(displayAttributes.outlineColor);
		}
		else
		{
			no_outline();
		}
		draw_rectangle(geometry.position.x, geometry.position.y, geometry.width, geometry.height);
		
		set_fill_color(displayAttributes.strColor);
		set_font_size(displayAttributes.strSize);
		if (displayAttributes.hasStrOutline)
		{
			set_outline_width(displayAttributes.strOutlineWidth);
			set_outline_color(displayAttributes.strOutlineColor);
		}
		else
		{
			no_outline();
		}

		draw_text(displayAttributes.str, geometry.position.x, geometry.position.y + geometry.height);
		pop_settings();
	}

	void UIButton::update()
	{
		wasHovered = isHovered;
		if (toBeExecutedOnHovered && !wasHovered)
		{
			displayAttributes = originalDisplayAttributes;
		}
		isHovered = false;
	}

	void UIButton::onHovered()
	{
		if (wasHovered && toBeExecutedOnHovered)
		{
			toBeExecutedOnHovered();
		}

		isHovered = true;
	}

	void UIButton::onClicked()
	{
		if (toBeExecutedOnClicked)
		{
			toBeExecutedOnClicked();
		}
	}

	void UIButton::allocateOnHovered(std::function<void()> newFunction)
	{
		toBeExecutedOnHovered = newFunction;
	}

	void UIButton::allocateOnClicked(std::function<void()> newFunction)
	{
		toBeExecutedOnClicked = newFunction;
	}

	void UIButton::setDisplayAttributes(const ButtonDisplayAttributes& newAttributes)
	{
		displayAttributes = newAttributes;
	}

	ButtonGeometry UIButton::getGeometry() const noexcept
	{
		return geometry;
	}
}
