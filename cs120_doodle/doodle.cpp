/*
	doodle.cpp

	GAM100, Fall 2019

	JoonHo Hwang wrote this all
	Sunghwan Cho
	Duhwan Kim

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/

#include <doodle/doodle.hpp>
#include "Game.h"
#include "global.h"
#include "InGameUI.h"

using namespace Enline;
using namespace doodle;

int main(void)
{
	create_window(800, 600);
	set_frame_of_reference(FrameOfReference::LeftHanded_OriginTopLeft);
	set_callback_mouse_pressed([](MouseButtons button) { game.onMousePressed(button); });

#if !defined(_DEBUG)
	// DigiPen logo splash
	unsigned short framesToDisplayLogo = LOGO_DISPLAY_FRAME;
	Texture digipenLogo;
	digipenLogo.LoadFromPNG("digipenLogo.png");
	set_texture_mode(RectMode::Center);

	while (!is_window_closed() && framesToDisplayLogo-- > 0)
	{
		update_window();
		clear_background(255);
		push_settings();
		draw_texture(digipenLogo, 400, 300);
		set_fill_color(0);
		set_font_size(18);
		draw_text("All content (C) 2019 DigiPen(USA) Corporation, all rights reserved.", 15.f, 550.f);
		pop_settings();
	}
	// DigiPen logo splash end
#endif

	while (!is_window_closed())
    {
        update_window();
        clear_background(GREY);
		game.update();
		game.draw();
    }

    return 0;
}
